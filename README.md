# Pizza App

A small app to customize pizzas

## Tech:

React, Styled Components, Typescript

## Approach

Only React, context api and hooks were used to guarantee the functionalities of the challenge below:

```
App specifications:
The app should allow the user to create his own pizza.
Pizza cost will be calculated depending on the size, crust type and amount of extra toppings.
User should first pick the size of the pizza:
> Small ($8)
> Medium ($10)
> Large ($12).
User should then be able to pick the crust type:
> Thin (+$2)
> Thick (+$4).
Then the user should be presented a list of ingredients with a photo and name to pick as toppings.
The user should be able to add from [0-3] ingredients from that list without any additional cost for the pizza.
The user may add more ingredients, but each new addition after the third one costs $0.50.
The user can't repeat ingredients.
Maximum ingredients for each pizza is:
> 5 for small
> 7 for medium
> 9 for large.
After the pizza is done, user should see a confirmation screen with his pizza and all detailed information.
Screens:
1. Choose your size
1. Choose your crust
2. Choose your toppings
3. Check your custom pizza
Available toppings:
- Pepperoni
- Mushrooms
- Onions
- Sausage
- Bacon
- Extra cheese
- Black olives
- Green peppers
- Pineapple
- Spinach
```

---

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
