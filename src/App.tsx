import React from 'react';
import { ThemeProvider } from 'styled-components';
import { lightTheme, GlobalStyle } from './components/ui-themes';
import { PizzaProvider } from './contexts/PizzaProvider';
import { Routes } from './pages';

const App: React.FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <PizzaProvider>
        <GlobalStyle />
        <Routes />
      </PizzaProvider>
    </ThemeProvider>
  );
};

export default App;
