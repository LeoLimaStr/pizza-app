import styled from 'styled-components';
import is, { isNot } from 'typescript-styled-is';
import {
  baseSpacing,
  getColorByVariant,
  getValueByMapSize,
} from '../ui-themes';

interface IProps {
  variant: string;
  size: string;
  disabled: boolean;
  [key: string]: any;
}

const sizeMap = ['small', 'medium', 'large'];
const paddingSize = ['11px 16px', '16px', '24px'];
const borderRadius = ['6px', '8px', '12px'];
const fontSize = ['14px', '16px', '20px'];
const borderWidth = ['1px', '1px', '2px'];
const borderBottomWidth = ['2px', '3px', '4px'];

const variantsMap = ['primary', 'secondary', 'warning', 'info'];
const fontColorVariant = ['white', 'white', 'white', 'white'];
const bgVariant = ['success500', 'error500', 'warning500', 'info500'];
const borderVariant = ['success700', 'error700', 'warning700', 'info700'];

export const Button = styled.button<IProps>`
  display: inline-block;
  padding: ${getValueByMapSize(sizeMap, paddingSize)};
  text-decoration: none;
  box-sizing: border-box;
  outline: none;
  font-family: 'Itim', cursive;
  font-size: ${getValueByMapSize(sizeMap, fontSize)};
  text-align: center;
  font-style: normal;
  font-weight: bold;
  letter-spacing: 0.05em;
  text-transform: lowercase;
  color: ${getColorByVariant(variantsMap, fontColorVariant)};
  background: ${getColorByVariant(variantsMap, bgVariant)};
  border-radius: ${getValueByMapSize(sizeMap, borderRadius)};
  border: ${getValueByMapSize(sizeMap, borderWidth)} solid
    ${getColorByVariant(variantsMap, bgVariant)};
  border-bottom-width: ${getValueByMapSize(sizeMap, borderBottomWidth)};
  border-bottom-color: ${getColorByVariant(variantsMap, borderVariant)};

  ${isNot('disabled')`
    cursor: pointer;
    :hover {
      transform: scale(1.05);
    }
    :active {
      transform: scale(0.95);
    }
  `}

  ${is('disabled')`
    background: #c3c3c3;
    border-color: #9c9b9b;
  `};

  & {
    ${baseSpacing}
  }
`;

Button.defaultProps = {
  variant: variantsMap[0],
  size: sizeMap[2],
  disabled: false,
};
