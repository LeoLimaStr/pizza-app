import React from 'react';
import { Wrapper } from '../ui-layouts';
import { IProps } from '../../utils/constants';

export const Card = ({ children, ...props }: IProps) => (
  <Wrapper
    margin="0"
    borderRadius="8px"
    marginBottom="20px"
    xsMinWidth="280px"
    smMinWidth="220px"
    xsMinHeight="120px"
    smMinHeight="250px"
    background="white"
    padding="16px"
    display="flex"
    xsFlexDirection="row"
    smFlexDirection="column"
    justifyContent="center"
    alignItems="center"
    cursor="pointer"
    shadowOnHover
    {...props}
  >
    {children}
  </Wrapper>
);
