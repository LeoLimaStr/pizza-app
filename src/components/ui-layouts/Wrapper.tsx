import styled from 'styled-components';
import {
  baseSizes,
  baseSpacing,
  baseDisplay,
  baseText,
  basePosition,
  baseBackground,
  baseBorder,
  baseElevator,
  getProp,
} from '../ui-themes';

export const Wrapper = styled.div`
  margin: 0 auto;
  padding: 0 24px;
  cursor: ${getProp('cursor')};

  & {
    ${baseBackground}
    ${baseSizes}
    ${baseSpacing}
    ${baseDisplay}
    ${baseText}
    ${basePosition}
    ${baseBorder}
    ${baseElevator}
  }
`;
