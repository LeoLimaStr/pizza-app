import styled from 'styled-components';
import { baseSizes, baseSpacing } from '../ui-themes';

export const Picture = styled.img`
  & {
    ${baseSizes}
    ${baseSpacing}
  }
`;
