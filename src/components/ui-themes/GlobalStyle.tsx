import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';

export const GlobalStyle = createGlobalStyle`
  ${normalize}

  * {box-sizing: border-box;}

  a {
    text-decoration: none;
  }
  
  h1,h2,h3,h4,h5,h6,p,fieldset{
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
  }
`;
