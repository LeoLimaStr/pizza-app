import { css } from 'styled-components';
import { getProp, getColorByProp } from '../utils';

/**
 * Decorate component with this base to accept this props
 * e.g.: <Wrapper background="#ccc" />
 *  */
export const baseBackground = css`
  background: ${getColorByProp('background')};
  background-image: ${getProp('backgroundImage')};
  background-position-x: ${getProp('backgroundPositionX')};
  background-position-y: ${getProp('backgroundPositionY')};
  background-repeat: ${getProp('backgroundRepeat')};
  background-size: ${getProp('backgroundSize')};
`;
