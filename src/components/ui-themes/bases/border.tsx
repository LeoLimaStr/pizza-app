import { css } from 'styled-components';
import { getProp } from '../utils';

/**
 * Decorate component with this base to accept this props
 * e.g.: <Wrapper borderRadius="8px" />
 *  */
export const baseBorder = css`
  border: ${getProp('border')};
  border-radius: ${getProp('borderRadius')};
`;
