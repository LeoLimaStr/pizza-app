import { css } from 'styled-components';
import is from 'typescript-styled-is';
import { sizes, minWidth } from './sizes';
import { getProp } from '../utils';

/**
 * Decorate component with this base to accept this props
 * e.g.: <Wrapper display="flex" />
 *  */
export const baseDisplay = css`
  flex: ${getProp('flex')};
  display: ${getProp('display')};
  flex-wrap: ${getProp('flexWrap')};
  flex-direction: ${getProp('flexDirection', 'column')};
  justify-content: ${getProp('justifyContent')};
  align-items: ${getProp('alignItems')};
  visibility: ${getProp('visibility')};
  overflow: ${getProp('overflow')};

  ${sizes.map(
    (s, i) => css`
      ${is(`${s}Display`)`
        @media (min-width: ${minWidth[i]}px) {
          display: ${getProp(`${s}Display`)};
        }
      `}
      ${is(`${s}FlexDirection`)`
        @media (min-width: ${minWidth[i]}px) {
          flex-direction: ${getProp(`${s}FlexDirection`)};
        }
      `}
      ${is(`${s}JustifyContent`)`
        @media (min-width: ${minWidth[i]}px) {
          justify-content: ${getProp(`${s}JustifyContent`)};
        }
      `}
    `
  )}
`;
