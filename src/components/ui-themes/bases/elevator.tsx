import { css } from 'styled-components';
import is from 'typescript-styled-is';

/**
 * Decorate component with this base to accept this props
 * e.g.: <Wrapper shadowOnHover />
 *  */
export const baseElevator = css`
  transition: all 0.2s ease-out;
  ${is('shadowOnHover')`
    :hover {
      transform: translate(0, -4px);
      box-shadow: 0px 4px 15px rgba(29, 58, 100, 0.1);
    }
  `}
`;
