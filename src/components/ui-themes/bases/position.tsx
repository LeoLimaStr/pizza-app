import { css } from 'styled-components';
import { getProp } from '../utils';

/**
 * Decorate component with this base to accept this props
 * e.g.: <Wrapper position="relative" />
 *  */
export const basePosition = css`
  position: ${getProp('position')};
  top: ${getProp('top')};
  right: ${getProp('right')};
  left: ${getProp('left')};
  bottom: ${getProp('bottom')};
  transform: ${getProp('transform')};
`;
