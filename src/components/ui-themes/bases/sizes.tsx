import { css } from 'styled-components';
import is from 'typescript-styled-is';
import { getProp } from '../utils';

export const sizes = ['xs', 'sm', 'md', 'lg'];
export const minWidth = ['0', '768', '992', '1200'];

/**
 * Decorate component with this base to accept this props
 * e.g.: <Wrapper width="100%" />
 *  */
export const baseSizes = css`
  height: ${getProp('height')};
  min-height: ${getProp('minHeight')};
  max-height: ${getProp('maxHeight')};

  width: ${getProp('width')};
  min-width: ${getProp('minWidth')};
  max-width: ${getProp('maxWidth')};

  ${sizes.map(
    (s, i) => css`
      ${is(`${s}Width`)`
        @media (min-width: ${minWidth[i]}px) {
          width: ${getProp(`${s}Width`)};
        }
      `}

      ${is(`${s}Height`)`
        @media (min-width: ${minWidth[i]}px) {
          height: ${getProp(`${s}Height`)};
        }
      `}

      ${is(`${s}MaxWidth`)`
        @media (min-width: ${minWidth[i]}px) {
          max-width: ${getProp(`${s}MaxWidth`)};
        }
      `}

      ${is(`${s}MinWidth`)`
        @media (min-width: ${minWidth[i]}px) {
          min-width: ${getProp(`${s}MinWidth`)};
        }
      `}

      ${is(`${s}MinHeight`)`
        @media (min-width: ${minWidth[i]}px) {
          min-height: ${getProp(`${s}MinHeight`)};
        }
      `}
    `
  )}
`;
