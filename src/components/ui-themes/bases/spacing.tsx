import { css } from 'styled-components';
import is from 'typescript-styled-is';
import { sizes, minWidth } from './sizes';
import { getProp } from '../utils';

/**
 * Decorate component with this base to accept this props
 * e.g.: <Wrapper paddingVertical="20px" />
 *  */
export const baseSpacing = css`
  padding: ${getProp('padding')};
  padding-top: ${getProp('paddingTop')};
  padding-right: ${getProp('paddingRight')};
  padding-bottom: ${getProp('paddingBottom')};
  padding-left: ${getProp('paddingLeft')};

  ${({ paddingVertical }) =>
    paddingVertical &&
    `
    padding-bottom: ${paddingVertical};
    padding-top: ${paddingVertical};
  `}

  ${({ paddingHorizontal }) =>
    paddingHorizontal &&
    `
    padding-left: ${paddingHorizontal};
    padding-right: ${paddingHorizontal};
  `}

  margin: ${getProp('margin')};
  margin-top: ${getProp('marginTop')};
  margin-right: ${getProp('marginRight')};
  margin-bottom: ${getProp('marginBottom')};
  margin-left: ${getProp('marginLeft')};

  ${({ marginVertical }) =>
    marginVertical &&
    `
    margin-bottom: ${marginVertical};
    margin-top: ${marginVertical};
  `}

  ${({ marginHorizontal }) =>
    marginHorizontal &&
    `
    margin-left: ${marginHorizontal};
    margin-right: ${marginHorizontal};
  `}

  ${sizes.map(
    (s, i) => css`
      ${is(`${s}PaddingHorizontal`)`
        @media (min-width: ${minWidth[i]}px) {
          padding-left: ${getProp(`${s}PaddingHorizontal`)};
          padding-right: ${getProp(`${s}PaddingHorizontal`)};
        }
      `}
      ${is(`${s}PaddingVertical`)`
        @media (min-width: ${minWidth[i]}px) {
          padding-top: ${getProp(`${s}PaddingVertical`)};
          padding-bottom: ${getProp(`${s}PaddingVertical`)};
        }
      `}
      ${is(`${s}PaddingTop`)`
        @media (min-width: ${minWidth[i]}px) {
          padding-top: ${getProp(`${s}PaddingTop`)};
        }
      `}
      ${is(`${s}PaddingRight`)`
        @media (min-width: ${minWidth[i]}px) {
          padding-right: ${getProp(`${s}PaddingRight`)};
        }
      `}
      ${is(`${s}PaddingBottom`)`
        @media (min-width: ${minWidth[i]}px) {
          padding-bottom: ${getProp(`${s}PaddingBottom`)};
        }
      `}
      ${is(`${s}Padding`)`
        @media (min-width: ${minWidth[i]}px) {
          padding: ${getProp(`${s}Padding`)};
        }
      `}

      ${is(`${s}MarginHorizontal`)`
        @media (min-width: ${minWidth[i]}px) {
          margin-left: ${getProp(`${s}MarginHorizontal`)};
          margin-right: ${getProp(`${s}MarginHorizontal`)};
        }
      `}

      ${is(`${s}Margin`)`
        @media (min-width: ${minWidth[i]}px) {
          margin: ${getProp(`${s}Margin`)};
        }
      `}
      ${is(`${s}MarginTop`)`
        @media (min-width: ${minWidth[i]}px) {
          margin-top: ${getProp(`${s}MarginTop`)};
        }
      `}
      ${is(`${s}MarginBottom`)`
        @media (min-width: ${minWidth[i]}px) {
          margin-bottom: ${getProp(`${s}MarginBottom`)};
        }
      `}
      ${is(`${s}MarginLeft`)`
        @media (min-width: ${minWidth[i]}px) {
          margin-left: ${getProp(`${s}MarginLeft`)};
        }
      `}
      ${is(`${s}MarginRight`)`
        @media (min-width: ${minWidth[i]}px) {
          margin-right: ${getProp(`${s}MarginRight`)};
        }
      `}
    `
  )}
`;
