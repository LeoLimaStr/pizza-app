import { css } from 'styled-components';
import is from 'typescript-styled-is';
import { sizes, minWidth } from './sizes';
import { getProp, getColorByProp } from '../utils';

/**
 * Decorate component with this base to accept this props
 * e.g.: <Text textAlign="center" />
 *  */
export const baseText = css`
  text-align: ${getProp('textAlign')};
  text-decoration: ${getProp('textDecoration')};
  text-transform: ${getProp('textTransform')};
  font-size: ${getProp('fontSize')};
  font-style: ${getProp('fontStyle')};
  font-weight: ${getProp('fontWeight')};
  color: ${getColorByProp('fontColor')};
  letter-spacing: ${getProp('letterSpacing')};
  line-height: ${getProp('lineHeight')};
  vertical-align: ${getProp('verticalAlign')};
  word-break: ${getProp('wordBreak')};

  ${sizes.map(
    (s, i) => css`
      ${is(`${s}TextAlign`)`
        @media (min-width: ${minWidth[i]}px) {
          text-align: ${getProp(`${s}TextAlign`)};
        }
      `}
    `
  )}
`;
