export { GlobalStyle } from './GlobalStyle';

export { baseSpacing } from './bases/spacing';
export { baseSizes } from './bases/sizes';
export { baseText } from './bases/texting';
export { baseDisplay } from './bases/display';
export { basePosition } from './bases/position';
export { baseBackground } from './bases/background';
export { baseElevator } from './bases/elevator';
export { baseBorder } from './bases/border';

export * from './themes';
export * from './utils';
