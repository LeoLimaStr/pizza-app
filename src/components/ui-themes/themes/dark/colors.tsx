// Pizza
export const primaryRed = '#d4202e';

// Feed-back
export const success500 = '#85b24d';
export const success700 = '#628739';

export const warning700 = '#ba8326';
export const warning500 = '#fbaf35';

export const error700 = '#ae3144';
export const error500 = '#ed475f';

// new colors
export const basicWhite = '#FFFFFF';
export const basicBlack = '#000000';
export const newGrayDark = '#333D4D';
export const newGrayDefault = '#88919D';
export const newGrayLighter = '#E7ECF2';
export const newGrayLight = '#F0F6F9';
export const primaryDark = '#A73828';
export const primaryDefault = '#FB5F51';
export const primaryLight = '#FFF7F5';
export const secondDark = '#16535B';
export const secondDefault = '#00A78D';
export const secondLight = '#ECFAFA';
// old colors
export const error800 = '#A93131';
// export const error500 = '#CA4848';
export const error200 = '#F4DADA';
export const success800 = '#4C7318';
// export const success500 = '#5F901F';
export const success200 = '#E0F3C7';
export const warning800 = '#C86E00';
// export const warning500 = '#EC870C';
export const warning200 = '#F7E6D1';
export const black = '#000000';
export const gray700 = '#323232';
export const gray500 = '#636363';
export const gray300 = '#848484';
export const gray200 = '#CDCDCD';
export const gray100 = '#E5E5E5';
export const gray50 = '#F5F5F5';
export const white = '#FFFFFF';
export const red700 = '#DB5950';
export const red600 = '#7C3E3E';
export const red500 = '#FF8674';
export const red300 = '#FF9E8F';
export const red100 = '#FAF8F7';
export const vine600 = '#894844';
export const vine500 = '#AB5C57';
export const vine100 = '#F6EEEE';
export const green900 = '#16535B';
export const green500 = '#00816D';
export const green300 = '#00A78D';
export const green200 = '#A8E2D9';
export const green100 = '#EFFAF8';
export const straw900 = '#BEB8AF';
export const straw700 = '#DAD1C2';
export const straw500 = '#F2EAE3';
export const straw100 = '#F9F8F3';
export const transparent = 'transparent';
