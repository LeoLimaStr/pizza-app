import * as lightColors from './light/colors';
import * as darkColors from './dark/colors';
import * as spacing from './light/spacing';

export const lightTheme = {
  colors: lightColors,
  spacing,
};

export const darkTheme = {
  colors: darkColors,
  spacing,
};
