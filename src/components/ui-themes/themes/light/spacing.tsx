export const xs = '360px';
export const sm = '768px';
export const md = '992px';
export const lg = '1200px';
