interface IProps {
  theme?: { colors: { [key: string]: string } };
  variant?: string;
  size?: string;
  [key: string]: any;
}

export const getColor = (color: string) => (props: IProps) => {
  return props?.theme?.colors[color] || 'black';
};

export const getValueByMapVariant = (map: Array<any>, values: Array<any>) => (
  props: IProps
) => values[map.findIndex((s) => s === props?.variant) || 0];

export const getColorByVariant = (
  map: Array<string>,
  values: Array<string>
) => (props: IProps) => {
  return props?.theme?.colors[
    values[map.findIndex((s) => s === props?.variant)]
  ]
    ? props?.theme?.colors[values[map.findIndex((s) => s === props.variant)]]
    : 'black';
};

export const getValueByMapSize = (
  map: Array<string>,
  values: Array<string>
) => (props: IProps) => values[map.findIndex((s) => s === props.size) || 0];

export const getValueByLevel = (values: Array<string>) => (
  props: IProps
): string => {
  return values[props.level - 1];
};

export const getColorByProp = (propName: string, defaultValue: any = null) => (
  props: IProps
) => {
  if (!props[propName]) {
    return defaultValue;
  }

  return props?.theme?.colors[props[propName]] || props[propName];
};

export const getProp = (propName: string, defaultValue: any = undefined) => (
  props: IProps
) => {
  return props[propName] || defaultValue;
};
