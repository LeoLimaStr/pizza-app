import styled from 'styled-components';
import { baseSpacing, getColorByProp, getProp, baseText } from '../ui-themes';
import { getValueByLevel } from './utils';

const fontSizeDesktop = ['42px', '34px', '26px', '22px', '18px', '14px'];
const lineHeightDesktop = ['51px', '41px', '32px', '150%', '150%', '150%'];
const letterSpacing = ['0', '0', '0', '0', '0', '0.1em'];

const fontSizeMobile = ['32px', '28px', '22px', '18px', '16px', '14px'];
const lineHeigtMobile = ['39px', '34px', '27px', '22px', '150%', '150%'];

interface IProps {
  level: number;
  [key: string]: any;
}

export const Heading = styled.h1<IProps>`
  font-family: 'Itim', cursive;
  font-weight: bold;
  font-size: ${getValueByLevel(fontSizeMobile)};
  line-height: ${getValueByLevel(lineHeigtMobile)};
  letter-spacing: ${getValueByLevel(letterSpacing)};
  text-align: ${getProp('textAlign')};
  border-bottom: ${getProp('borderBottom')};
  color: ${getColorByProp('color')};

  a {
    color: inherit;
    text-decoration: none;
  }

  @media (min-width: 768px) {
    font-size: ${getValueByLevel(fontSizeDesktop)};
    line-height: ${getValueByLevel(lineHeightDesktop)};
  }

  & {
    ${baseSpacing};
    ${baseText};
  }
`;
