import styled from 'styled-components';
import {
  baseSpacing,
  getColorByProp,
  getProp,
  baseText,
  getValueByMapSize,
} from '../ui-themes';
import { IProps } from '../../utils/constants';

const sizeMap = ['large', 'medium', 'small'];
const fontSizeDesktop = ['20px', '16px', '14px'];
const fontSizeMobile = ['18px', '16px', '14px'];
const textRendering = ['optimizeLegibility', '', ''];

export const Paragraph = styled.p<IProps>`
  font-family: 'Itim', cursive;
  font-size: ${getValueByMapSize(sizeMap, fontSizeMobile)};
  line-height: 175%;
  color: ${getColorByProp('color')};
  text-align: ${getProp('textAlign')};
  text-rendering: ${getValueByMapSize(sizeMap, textRendering)};

  & {
    ${baseSpacing};
    ${baseText};
  }

  @media (min-width: 768px) {
    font-size: ${getValueByMapSize(sizeMap, fontSizeDesktop)};
  }
`;

Paragraph.defaultProps = {
  size: sizeMap[0],
};
