const max6 = (value: number) => Math.min(Math.abs(value), 6);

interface IProps {
  level: number;
  [key: string]: any;
}

export const getValueByLevel = (values: Array<string>) => ({
  level,
}: IProps): string => {
  return values[max6(level - 1)];
};
