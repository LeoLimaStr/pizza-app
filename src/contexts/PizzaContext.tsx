import React from 'react';
import { ICONFIG, IPIZZA } from '../utils/constants';

type ContextProps = {
  config: ICONFIG | undefined;
  pizza: IPIZZA | undefined;
  step: number;
  changeStep: Function;
  chooseSize: Function;
  chooseCrustType: Function;
  toggleIngredient: Function;
  reset: Function;
};

export const PizzaContext = React.createContext<ContextProps>({
  step: 0,
  config: undefined,
  pizza: undefined,
  changeStep: (previous: boolean = false, pizza?: any) => {},
  chooseSize: (size: string) => {},
  chooseCrustType: (crust: string) => {},
  toggleIngredient: (ingredient: string) => {},
  reset: () => {},
});
