import React from 'react';
import { PizzaContext } from './PizzaContext';
import { useSteps, usePizzaConfig } from '../hooks';
import { IProps, IPIZZA } from '../utils/constants';

export const PizzaProvider = ({ children }: IProps) => {
  const { config } = usePizzaConfig();
  const { step, changeStep, resetSteps } = useSteps();
  const [pizza, setPizza] = React.useState<IPIZZA | undefined>(undefined);

  const chooseSize = (size: string) =>
    setPizza({ crust: pizza?.crust, ingredients: [], size });
  const chooseCrustType = (crust: string) =>
    setPizza({ size: pizza?.size, ingredients: pizza?.ingredients, crust });
  const toggleIngredient = (ingredient: string) => {
    const reachLimit =
      (pizza?.ingredients || []).length >=
      (config?.sizes[pizza?.size || 'large'].maxIngredients || 0);

    const newList = (pizza?.ingredients || []).includes(ingredient)
      ? (pizza?.ingredients || []).filter((e) => e !== ingredient)
      : !reachLimit
      ? [...(pizza?.ingredients || []), ingredient]
      : [...(pizza?.ingredients || [])];
    setPizza({
      size: pizza?.size,
      crust: pizza?.crust,
      ingredients: newList,
    });
  };

  const reset = () => {
    setPizza(undefined);
    resetSteps();
  };

  return (
    <PizzaContext.Provider
      value={{
        step,
        config,
        pizza,
        chooseSize,
        chooseCrustType,
        toggleIngredient,
        changeStep,
        reset,
      }}
    >
      {children}
    </PizzaContext.Provider>
  );
};
