import React from 'react';
import { configPizzaApi } from '../services';
import { ICONFIG } from '../utils/constants';

export function usePizzaConfig() {
  const [config, setConfig] = React.useState<ICONFIG | undefined>();

  React.useEffect(() => {
    async function loadConfig() {
      setConfig(await configPizzaApi());
    }
    loadConfig();
  }, []);

  return { config };
}
