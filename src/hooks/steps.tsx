import React from 'react';
import { IPIZZA } from '../utils/constants';

export function useSteps() {
  const [step, setStep] = React.useState(0);

  const changeStep = React.useCallback(
    (previous: boolean = false, pizza?: IPIZZA) => {
      setStep(previous ? step - 1 : step + 1);
    },
    [step, setStep]
  );

  const resetSteps = React.useCallback(() => {
    setStep(0);
  }, [setStep]);

  return { step, changeStep, resetSteps };
}
