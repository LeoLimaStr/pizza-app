import React from 'react';
import { Wrapper } from '../../components/ui-layouts';
import { Paragraph } from '../../components/ui-typograph';
import { PizzaContext } from '../../contexts/PizzaContext';
import {
  PizzaArt,
  AnimatedHeading,
  AnimatedButton,
  ItemList,
  Page,
} from '../sharedStyles';

const Checkout: React.FC = () => {
  const { config, pizza, changeStep, reset } = React.useContext(PizzaContext);

  // get quantity of ingredients
  const choosedIngredients = (pizza?.ingredients || []).length;

  // calculate quantity additional ingredients
  const additionalIngredients =
    choosedIngredients > (config?.quantityFreeingredients || 3)
      ? choosedIngredients - (config?.quantityFreeingredients || 3)
      : 0;

  // calculate final price
  const total =
    (config?.sizes[pizza?.size || 'large'].price || 0) +
    (config?.crustType[pizza?.crust || 'thin'] || 0) +
    additionalIngredients * (config?.additionalCostIgredients || 0);

  return (
    <Page>
      <AnimatedHeading level={1} textAlign="center" marginBottom="48px">
        Check your custom pizza
      </AnimatedHeading>
      <Wrapper padding="0" margin="0" maxWidth="100px" textAlign="center">
        <PizzaArt src="/images/pizza.svg" width="50px" />
      </Wrapper>
      <Wrapper
        display="flex"
        justifyContent="center"
        width="100%"
        maxWidth="320px"
        padding="0"
        fontColor="black"
        marginBottom="48px"
      >
        <ItemList>
          <Paragraph>Size: {pizza?.size}</Paragraph>
          <Paragraph>$ {config?.sizes[pizza?.size || 'large'].price}</Paragraph>
        </ItemList>
        <ItemList>
          <Paragraph>Crust: {pizza?.crust}</Paragraph>
          <Paragraph>$ {config?.crustType[pizza?.crust || 'thin']}</Paragraph>
        </ItemList>
        <Wrapper margin="0" padding="0">
          <Paragraph margin="8px 16px">Ingredients</Paragraph>
          {(pizza?.ingredients || []).map((key, i) => (
            <ItemList key={key}>
              <Paragraph size="small">
                {key}
                {i + 1 > (config?.quantityFreeingredients || 3) &&
                  ' (Additional)'}
              </Paragraph>
              <Paragraph size="small">
                {i + 1 > (config?.quantityFreeingredients || 3)
                  ? `$ ${config?.additionalCostIgredients}`
                  : '--'}
              </Paragraph>
            </ItemList>
          ))}
        </Wrapper>
        <ItemList>
          <Paragraph size="large">Total:</Paragraph>
          <Paragraph size="large">$ {total}</Paragraph>
        </ItemList>
      </Wrapper>
      <Wrapper display="flex" flexDirection="row">
        <AnimatedButton
          onClick={() => changeStep(true)}
          variant="warning"
          marginRight="24px"
        >
          Previous
        </AnimatedButton>
        <AnimatedButton onClick={() => reset()} marginRight="24px">
          Finish
        </AnimatedButton>
      </Wrapper>
    </Page>
  );
};

export default Checkout;
