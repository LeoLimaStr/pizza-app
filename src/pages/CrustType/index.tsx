import React from 'react';
import { Wrapper, Card } from '../../components/ui-layouts';
import { Heading } from '../../components/ui-typograph';
import { PizzaContext } from '../../contexts/PizzaContext';
import {
  PizzaArt,
  AnimatedHeading,
  AnimatedButton,
  Page,
} from '../sharedStyles';

const CrustType: React.FC = () => {
  const { config, chooseCrustType, pizza, changeStep } = React.useContext(
    PizzaContext
  );

  return (
    <Page>
      <AnimatedHeading level={1} textAlign="center" marginBottom="48px">
        Choose the Crust Type
      </AnimatedHeading>
      <Wrapper
        display="flex"
        smFlexDirection="row"
        justifyContent="center"
        width="100%"
        padding="0"
        fontColor="black"
        marginBottom="48px"
      >
        {Object.keys(config?.crustType || []).map((key, i) => (
          <Card
            key={key}
            background={pizza?.crust === key ? 'success500' : 'white'}
            onClick={() => chooseCrustType(key)}
            margin="0 12px"
          >
            <Wrapper padding="0" margin="0" maxWidth="100px" textAlign="center">
              <PizzaArt src="/images/crust.svg" width={`${(i + 1) * 20}%`} />
            </Wrapper>
            <Heading level={4} as="h2">
              {key}
            </Heading>
            <Wrapper padding="0">$ {config?.crustType[key]}</Wrapper>
          </Card>
        ))}
      </Wrapper>
      <Wrapper display="flex" flexDirection="row">
        <AnimatedButton
          onClick={() => changeStep(true)}
          variant="warning"
          marginRight="24px"
        >
          Previous
        </AnimatedButton>
        <AnimatedButton
          disabled={!pizza?.crust}
          onClick={() => changeStep(false, pizza)}
        >
          Next
        </AnimatedButton>
      </Wrapper>
    </Page>
  );
};

export default CrustType;
