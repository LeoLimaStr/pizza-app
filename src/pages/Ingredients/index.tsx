import React from 'react';
import { Wrapper, Card } from '../../components/ui-layouts';
import { Heading, Paragraph } from '../../components/ui-typograph';
import { PizzaContext } from '../../contexts/PizzaContext';
import {
  PizzaArt,
  AnimatedHeading,
  AnimatedButton,
  Page,
} from '../sharedStyles';

const Ingredients: React.FC = () => {
  const { config, toggleIngredient, pizza, changeStep } = React.useContext(
    PizzaContext
  );

  // get quantity of ingredients
  const choosedIngredients = (pizza?.ingredients || []).length;

  // calculate quantity additional ingredients
  const additionalIngredients =
    choosedIngredients > (config?.quantityFreeingredients || 3)
      ? choosedIngredients - (config?.quantityFreeingredients || 3)
      : 0;

  return (
    <Page>
      <AnimatedHeading level={1} textAlign="center" marginBottom="24px">
        Choose the Ingredients
      </AnimatedHeading>
      <Paragraph>
        Free Ingredients{' '}
        {choosedIngredients < (config?.quantityFreeingredients || 3)
          ? choosedIngredients
          : config?.quantityFreeingredients || 3}
        /{config?.quantityFreeingredients}
      </Paragraph>
      <Paragraph marginBottom="24px">
        Additional Ingredients {additionalIngredients}/
        {config?.sizes[pizza?.size || 'large'].additionalIngredients || 0} ($
        {config?.additionalCostIgredients} each)
      </Paragraph>
      <Wrapper
        display="flex"
        flexWrap="wrap"
        smFlexDirection="row"
        justifyContent="center"
        width="100%"
        padding="0"
        fontColor="black"
        marginBottom="40px"
      >
        {config?.ingredients.map((key, i) => (
          <Card
            key={key}
            background={
              (pizza?.ingredients || []).includes(key) ? 'success500' : 'white'
            }
            onClick={() => toggleIngredient(key)}
            margin="0 12px"
          >
            <Wrapper padding="0" margin="0" minWidth="100px" textAlign="center">
              <PizzaArt src={`/images/ingredients/${key}.svg`} width={`50px`} />
            </Wrapper>
            <Wrapper padding="0">
              <Heading level={4} as="h2">
                {key}
              </Heading>
            </Wrapper>
          </Card>
        ))}
      </Wrapper>
      <Wrapper display="flex" flexDirection="row">
        <AnimatedButton
          onClick={() => changeStep(true)}
          variant="warning"
          marginRight="24px"
        >
          Previous
        </AnimatedButton>
        <AnimatedButton
          disabled={choosedIngredients < (config?.quantityFreeingredients || 3)}
          onClick={() => changeStep()}
        >
          Next
        </AnimatedButton>
      </Wrapper>
    </Page>
  );
};

export default Ingredients;
