import React from 'react';
import { Wrapper, Card } from '../../components/ui-layouts';
import { Heading } from '../../components/ui-typograph';
import { PizzaContext } from '../../contexts/PizzaContext';
import {
  PizzaArt,
  AnimatedHeading,
  AnimatedButton,
  Page,
} from '../sharedStyles';

const Sizes: React.FC = () => {
  const { config, chooseSize, pizza, changeStep } = React.useContext(
    PizzaContext
  );

  return (
    <Page>
      <AnimatedHeading level={1} textAlign="center" marginBottom="48px">
        Choose a Size
      </AnimatedHeading>
      <Wrapper
        display="flex"
        smFlexDirection="row"
        justifyContent="center"
        width="100%"
        padding="0"
        fontColor="black"
        marginBottom="48px"
      >
        {Object.keys(config?.sizes || []).map((key, i) => (
          <Card
            key={key}
            background={pizza?.size === key ? 'success500' : 'white'}
            onClick={() => chooseSize(key)}
            margin="0 12px"
          >
            <Wrapper padding="0" margin="0" maxWidth="100px" textAlign="center">
              <PizzaArt src="/images/pizza.svg" width={`${(i + 1) * 20}%`} />
            </Wrapper>
            <Heading level={4} as="h2">
              {key}
            </Heading>
            <Wrapper padding="0">$ {config?.sizes[key].price}</Wrapper>
          </Card>
        ))}
      </Wrapper>
      <AnimatedButton
        disabled={!pizza?.size}
        onClick={() => changeStep(false, pizza)}
      >
        Next
      </AnimatedButton>
    </Page>
  );
};

export default Sizes;
