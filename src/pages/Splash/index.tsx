import React from 'react';
import {
  PizzaArt,
  AnimatedHeading,
  AnimatedButton,
  Page,
} from '../sharedStyles';
import { PizzaContext } from '../../contexts/PizzaContext';

const Splash: React.FC = () => {
  const { changeStep } = React.useContext(PizzaContext);

  return (
    <Page>
      <AnimatedHeading level={1} letterSpacing="0.3em" marginBottom="48px">
        Pizza App
      </AnimatedHeading>
      <PizzaArt src="/images/pizza.svg" height="30vh" marginBottom="48px" />
      <AnimatedButton onClick={() => changeStep()}>Buy a pizza</AnimatedButton>
    </Page>
  );
};

export default Splash;
