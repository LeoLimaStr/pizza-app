import React, { lazy, Suspense } from 'react';
import { PizzaContext } from '../contexts/PizzaContext';
import { Wrapper } from '../components/ui-layouts';
import { SCREEN } from '../utils/constants';

const SplashScreen = lazy(() => import('./Splash'));
const SizesScreen = lazy(() => import('./Sizes'));
const CrustTypeScreen = lazy(() => import('./CrustType'));
const IngredientsScreen = lazy(() => import('./Ingredients'));
const CheckoutScreen = lazy(() => import('./Checkout'));

export const Routes: React.FC = () => {
  const { step } = React.useContext(PizzaContext);
  return (
    <Wrapper background="primaryRed" minHeight="100vh" padding="0">
      <Suspense fallback={null}>
        {step === SCREEN.SPLASH && <SplashScreen />}
        {step === SCREEN.SIZE && <SizesScreen />}
        {step === SCREEN.CRUST && <CrustTypeScreen />}
        {step === SCREEN.INGREDIENTS && <IngredientsScreen />}
        {step === SCREEN.CHECKOUT && <CheckoutScreen />}
      </Suspense>
    </Wrapper>
  );
};
