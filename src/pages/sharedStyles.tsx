import styled, { keyframes } from 'styled-components';
import is from 'typescript-styled-is';
import { Picture } from '../components/ui-picture';
import { Wrapper } from '../components/ui-layouts';
import { Heading } from '../components/ui-typograph';
import { Button } from '../components/ui-buttons';

const slideFwd = keyframes`
  0% {
    transform: scale(0);
  }
  100% {
    transform: scale(1) rotate(360deg);
  }
`;

const trackinInContract = keyframes`
  0% {
    letter-spacing: 1em;
    opacity: 0;
  }
  40% {
    opacity: 0.6;
  }
  100% {
    opacity: 1;
  }
`;

export const ItemList = styled(Wrapper)`
  border-bottom-width: 1px;
  border-bottom-style: dashed;
  border-bottom-color: #000;
  ${is('noborder')`
    border: 0;
  `}
  padding: 8px 16px;
  margin: 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const AnimatedHeading = styled(Heading)`
  animation: ${trackinInContract} 1s cubic-bezier(0.215, 0.61, 0.355, 1) both;
`;

export const AnimatedButton = styled(Button)`
  animation: ${trackinInContract} 1s cubic-bezier(0.215, 0.61, 0.355, 1) both;
`;

export const PizzaArt = styled(Picture)`
  animation: ${slideFwd} 0.45s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
`;

export const Page = styled(Wrapper)`
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  padding: 40px 0;
  margin: 0;
`;
