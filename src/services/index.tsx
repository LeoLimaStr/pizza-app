import { ICONFIG } from '../utils/constants';

export const configPizzaApi = () =>
  new Promise<ICONFIG>((resolve, _reject) =>
    resolve({
      sizes: {
        small: {
          price: 8,
          maxIngredients: 5,
          additionalIngredients: 2,
        },
        medium: {
          price: 10,
          maxIngredients: 7,
          additionalIngredients: 4,
        },
        large: {
          price: 12,
          maxIngredients: 9,
          additionalIngredients: 6,
        },
      },
      quantityFreeingredients: 3,
      additionalCostIgredients: 0.5,
      crustType: {
        thin: 2,
        thick: 4,
      },
      ingredients: [
        'Pepperoni',
        'Mushrooms',
        'Onions',
        'Sausage',
        'Bacon',
        'Extra cheese',
        'Black olives',
        'Green peppers',
        'Pineapple',
        'Spinach',
      ],
    })
  );
