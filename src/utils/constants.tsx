export enum SCREEN {
  SPLASH,
  SIZE,
  CRUST,
  INGREDIENTS,
  CHECKOUT,
}

export interface ICONFIG {
  sizes: { [key: string]: { [key: string]: number } };
  quantityFreeingredients: number;
  additionalCostIgredients: 0.5;
  crustType: { [key: string]: number };
  ingredients: string[];
}

export interface IProps {
  children: any;
  [key: string]: any;
}

export interface IPIZZA {
  size: string | undefined;
  crust: string | undefined;
  ingredients: string[] | undefined;
  // { [key: string]: number } | undefined;
}
